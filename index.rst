.. BVJ documentation master file, created by
   sphinx-quickstart on Mon May 25 22:28:02 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================
Schülermanagement
===============================

.. sidebar:: Dateien

    	:download:`BVJ_Admin_Tool <figure/BVJ_Admin_Tool.docx>`.
    	
    	:download:`BSW_Komponenten <figure/SW_Komponenten.png>`


.. toctree::
   :maxdepth: 2

   organisatorisches.rst
   entscheidungen.rst
   info.rst
   


.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

