Funktionalitäten
================


.. image:: figure/SW_Komponenten.png


Schülerdatenverwaltung
----------------------

- Zentrale Datenhaltung, Verschlüsselung
- Zugriff über Weboberfläche
- Mehrere Akteure/Nutzer, auch außerhalb des Schulnetzes

  - Datenschutz/ Datensicherheit/ Berechtigungen.

- Klassenlehrer alle Rechte, Akteur 2 Leserechte + Schreibrechte Eingabe Fehlzeiten. Rest nur Leserechte.


Fehlzeitenverwaltung
--------------------

- Fehlzeiten differenzieren nach Verspätungen, Stunden, Tage
- Eintragungen entschuldigt, unentschuldigt, Attest ja/nein.
- Abfragemöglichkeiten aller Art wie z.B. Auflistung aller unentschuldigten Fehltage inkl. Datum  und Erzegen eines Berichts.
  
- Regeln einbauen, dynamisch, um Regeln selbst definieren, modifizieren zu können

  - Nach 5 (dynamisch modifizierbar) unentschuldigten Fehltagen soll automatisch auf Attestpflicht hingewiesen werden.

    -  Automatisches Füllen einer Word-Briefvorlage mit den Adressdaten, Textbausteinen und Anzahl der Fehltage inkl. Datum.

  - Nach Attestpflichtdruck Hinweis auf ElternAnruf
  
    - Eingabemöglichkeiten mit Datum ob Eltern telefonisch erreicht oder nicht.
    
  - Nach jeweils 3 (dynamisch modifizierbar) Verspätungen soll auf Nachsitzen hingewiesen werden. Automatische Email an Verteiler/Einzeladresse mit den Namen der Schüler mit Datum des Tages Dienstag in der darauffolgenden Woche.


SchülerInformationsSystem (SISy) & Berichtssystem
-------------------------------------------------

- Eingabemöglichkeiten von Freihandtext zu jedem Schüler, fortlaufend mit Vorfallsnummer und Datum.
  - Vorfälle und Fehlverhalten
  
- Auswahlmöglichkeiten nach der Eingabe

  - Übernahme des Textes in eine Wordvorlage für einen Verweis inkl. Adressdaten des Schülers und anschließendem automatischen Drucken.
  
  - Übernahme des Textes in eine Wordvorlage für einen verschärften Verweis inkl. Adressdaten des Schülers und anschließendem automatischen Drucken.
  
  - Nach Auswahl automatisierte Weiterleitung des Vorfalls an diverse Emailverteiler bzw. einzelne Email-Adressen.
  
  - Alle Emails sollen im Betreff mit „10BJ1“ beginnen. Klassenbezeichnung dynamisch änderbar.


Teams
=====

Die Teams sind im Hinblick auf die die verschiedenen Funktionalitäten zu bilden -- jede Funtionalität wird von einem
Team abgebildet.


- Team 1: Frontend Schülerverwaltung
  - Berberich

- Team 2: Frontend Fehlzeitenverwaltung
  - Drexler

- Team 3: SIS



