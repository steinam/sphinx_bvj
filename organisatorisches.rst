Organisatorisches
=================

Vorüberlegungen
---------------

#. Arbeiten mit / Erlernen neuer Technologie (Verbindung zu PTP)
#. Nicht alle Vorstellungen von Herrn Lange müssen in der Form umgesetzt werden (Word)
#. Manche Tätigkeiten müssen teamübergreifend bearbeitet oder durch ein eigenes Team abgebildet werden (Datenbank) .
#. Erfolgreicher Abschluss steht nicht im Vordergrund bzw. ist in der zur Verfügung stehenden Zeit nicht möglich. 
#. Eventuell Übertragen des Projektes in das nächste Schuljahr



Akteure
-------

**Kunde** (Productowner): Herr Lange

**Scrummaster** für alle: Herr Steinam

**Developer** Schüler 11FI4, organisiert in Teams

**Team:**

- Team-Manager verantwortlich für

	- die Kommunikation mit den anderen Teams (z.B. Absprache über Schnittstellen)
	- die Kommunikation mit dem Kunden
	- Anfragen/Wünsche bzgl. der IT-Infrastruktur an den Scrummaster

- Team-Mitgieder sind verantwortlich für 

	- Aufbau der notwendigen Team-Infrastruktur
	- Dokumentation der teaminterne Prozesse (Kommunikation, Aufgabenmanagement, Bugmanagement)
	- Dokumentation der Ergebnisse (Konzepte, Dokumention des Quellcodes, Schnittstellendokumentation,  read-me, Diagramme)

  Diese sollte über die oben angesprochene Software abgebildet werden.


Teams
------

Die Teams sind im Hinblick auf die die verschiedenen Funktionalitäten zu bilden -- jede Funtionalität wird von einem Team abgebildet.


- Team 1: Frontend Schülerverwaltung
  - Berberich

- Team 2: Frontend Fehlzeitenverwaltung
  - Drexler

- Team 3: SIS
  - 

Teamarbeit
----------

Arbeiten mit Methoden moderner Softwareentwicklung

- Benutzen eines Vorgehensmodells (angepasst an den Schulalltag)
- Versionsverwaltung
- Projektmanagement (Zeitmanagement/Bugtracker/Features/....)
- Projektdokumentation (Wiki/API-Dokumentation/Diagramme zur Übersicht (UML)
- Integration von Tests und Metriken (falls möglich)
- Agiles Modell wünschenswert, aber vielleicht nicht unbedingt sinnvoll ?
- Nachvollziehbarkeit der Tätigkeiten einer jeden Person (was hat er gemacht)
- Die Arbeit eines Teammitglieds sollte nicht nur im Schreiben von Dokumentation bestehen.



Bewertung
---------

Das Projekt wird in Form einer Schulaufgabe nach der 3. Woche benotet. Dabei kommt es weniger auf den erfolgreichen Abschluss bzw. auf den Stand der Arbeit an, sondern auf das erfolgreiche Umsetzen der oben genannten Elemente der Teamarbeit. Weiterhin geht in die Note eine teaminterne bewertung der jeweiligen Teammitglieder ein.

Jedes Teammitglied wird in der 4. Woche zusätzlich eine Bewertung über seine übrigen Teammitglieder schreiben sowie ein persönliches Fazit ziehen.








